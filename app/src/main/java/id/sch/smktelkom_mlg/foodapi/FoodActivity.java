package id.sch.smktelkom_mlg.foodapi;

/**
 * Created by Root on 2/15/2018.
 */

public class FoodActivity {
    private String title;
    private String publisher;
    private String f2f_url;

    public FoodActivity(String t, String p, String f) {
        title = t;
        publisher = p;
        f2f_url = f;
    }

    public String getTitle() {
        return title;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getF2f_url() {
        return f2f_url;
    }
}

