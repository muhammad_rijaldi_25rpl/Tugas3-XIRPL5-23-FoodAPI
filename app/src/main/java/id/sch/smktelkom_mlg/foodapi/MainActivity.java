package id.sch.smktelkom_mlg.foodapi;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    String url = "http://food2fork.com/api/search?key=fdc7b505f29dd0f4b3652615d3d54c04&q=shredded%20chicken";
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    List<FoodActivity> foodClassList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setAdapter(adapter);
        getvolley();
    }

    public void getvolley() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                getJSON(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void getJSON(String request) {
        try {
            JSONObject object = new JSONObject(request);
            JSONArray jsonArray = object.getJSONArray("recipes");
            for (int x = 0; x < jsonArray.length(); x++) {
                JSONObject jObject = jsonArray.getJSONObject(x);

                FoodActivity food = new FoodActivity(jObject.getString("publisher"),
                        jObject.getString("title"),
                        jObject.getString("title"));
                foodClassList.add(food);
                adapter = new FoodAdapter(foodClassList);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(adapter);


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
