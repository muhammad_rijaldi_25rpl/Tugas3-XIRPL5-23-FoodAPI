package id.sch.smktelkom_mlg.foodapi;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Root on 2/15/2018.
 */

public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.Holder> {
    List<FoodActivity> foodClassList;

    public FoodAdapter(List<FoodActivity> halal) {
        foodClassList = halal;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_item_data, null);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        FoodActivity h = foodClassList.get(position);
        holder.tvTitle.setText("Title : " + h.getTitle());
        holder.tvPublisher.setText("Publisher : " + h.getPublisher());
        holder.tvf2f_url.setText("Url : " + h.getF2f_url());
    }

    @Override
    public int getItemCount() {
        return foodClassList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvPublisher;
        TextView tvf2f_url;

        public Holder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.textViewTitle);
            tvPublisher = itemView.findViewById(R.id.textViewpublisher);
            tvf2f_url = itemView.findViewById(R.id.textViewf2f_url);
        }
    }
}